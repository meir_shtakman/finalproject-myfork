//service: 
- accountsExist(accounts:IAccount[],tuples:[number,number][]);
- accountsActive(accounts:IAccount[]);
- accountsType(accounts:IAccount[],type:string);
- accountsCurrency(accounts:IAccount[],currency:string);
- allowTransfers(accounts:IAccount[],minBalance:number);

//validate:
- checkDigit(id_length:number,id:number);
- checkBalance(minimalBalance:number,balance:number);
- amountPositive(amount:number);
- sumAmount(tupels:[number,number][],balance:number);

