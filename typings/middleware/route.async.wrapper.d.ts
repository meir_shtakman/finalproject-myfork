import { RequestHandler } from "express";
declare const _default: (fn: RequestHandler) => RequestHandler;
export default _default;
