import { Request, Response, NextFunction } from "express";
export declare function validateBusinessModel(req: Request, res: Response, next: NextFunction): void;
