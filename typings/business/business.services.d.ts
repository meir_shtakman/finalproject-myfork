import { IBusiness } from "../types/types.js";
export declare function createBusinessAccount(business: IBusiness): Promise<any>;
export declare function getBusinessAccountById(accountId: number): Promise<any>;
