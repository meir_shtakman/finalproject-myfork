import { IAccount } from "../types/types.js";
export declare function createIndividualAccount(individual: Partial<IAccount>): Promise<any>;
export declare function getIndividualAccountById(accountId: number): Promise<any>;
