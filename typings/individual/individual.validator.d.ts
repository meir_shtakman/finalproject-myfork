import { Request, Response, NextFunction } from "express";
export declare function validateIndividualModel(req: Request, res: Response, next: NextFunction): void;
