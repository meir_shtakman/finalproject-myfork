import { ITransfer } from "../types/types.js";
export declare function transferB2B(payload: ITransfer): Promise<string>;
export declare function transferB2I(payload: ITransfer): Promise<string>;
export declare function transferF2B(payload: ITransfer): Promise<string>;
