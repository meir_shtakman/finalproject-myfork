export declare function generateID(): string;
export declare function getTimeString(): string;
export declare function convertCurrency(base: string, target: string, amount: number): Promise<number>;
