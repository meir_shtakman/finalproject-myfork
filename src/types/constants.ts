//errors
export const INVALID_FILED="Invalid filed"
export const INVALID_FILED_VALUE="Invalid filed value"
export const MISSING_REQUIRED_FIELD="Missing requiered field"
export const ACCOUNT_NOT_EXIST="Account does not exist"
export const ACCOUNT_ALREADY_EXIST="only 1 individual Account per person is allowed"
export const ACCOUNT_BALLANCE_LOW="Account ballance too low"
export const DATA_NOT_FOUND="Data not found"
export const SOMTHING_WENT_WRONG="somthing went wrong"
export const NOT_AUTHORIZED="you are not autorized to accsess this route" 


export const INVALID_AMOUNT_VALUE = "Invalid amount";
//ENUMS for types of transactions and req

//magic numbers (constants)
export const INDIVIDUAL_ID_LENGTH = 7; 
export const COMPANY_ID_LENGTH = 8;
export const MIN_INDIVIDUAL_BALANCE = 1000;
export const MIN_BUSINESS_BALANCE = 10000;
export const MIN_FAMILY_BALANCE = 5000;

