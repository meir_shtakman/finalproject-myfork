/* eslint-disable @typescript-eslint/no-unused-vars */
import { Request, Response, NextFunction } from "express";
import { validateAccountMandatoryFields } from "../account/account.validation.js";
import {INDIVIDUAL_ID_LENGTH,MISSING_REQUIRED_FIELD, INVALID_FILED} from '../types/constants.js';
import { IIndividual } from "../types/types.d.js";
import { validIndividualId } from "../utils/validationFunc.js";


export function validateIndividualModel(req:Request,res:Response,next:NextFunction):void {

    const {first_name,last_name,currency,account_id,individual_id,email=null,address=null,balance=0} = req.body;
    validateAccountMandatoryFields(currency as string,balance as number);
  
    if(!(first_name && last_name && currency && individual_id)){
        throw new Error(`${MISSING_REQUIRED_FIELD}`);
    }

    if (account_id !== undefined) {
        throw new Error(INVALID_FILED);
    }    

    validIndividualId(INDIVIDUAL_ID_LENGTH,individual_id as number);

    const account:Partial <IIndividual> = {first_name,last_name,currency,individual_id,email,address,balance,status:true};
    console.log("Iaccount valid!",account);
    req.accounts=[account];
    next()
}

